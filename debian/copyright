Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Linux-Inotify2
Source: http://software.schmorp.de/pkg/Linux-Inotify2.html

Files: *
Copyright: 2005-2021 Marc Lehmann <schmorp@schmorp.de,
                                   cpan@schmorp.de>
License: Perl
Comment: The original upstream license says: 'This module is licensed under
         the same terms as perl itself'. Perl uses GPL-1+ or Artistic.

Files: debian/*
Copyright: 2006-2007 Michael Bramer <grisu@debian.org>
           2008      Niko Tyni <ntyni@debian.org>
           2010      Franck Joncourt <franck@debian.org>
           2011      Hilko Bengen <bengen@debian.org>
           2014-2021 Joao Eriberto Mota Filho <eriberto@debian.org>
License: Perl

License: Perl
    This program is free software; you can redistribute it and/or modify
    it under the terms of either:
    .
    a) the GNU General Public License as published by the Free Software
       Foundation; either version 1, or (at your option) any later
       version, or
    .
    b) the "Artistic License" which comes with Perl.
    .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License version 1 can be found in `/usr/share/common-licenses/GPL-1'
 and the Artistic Licence in `/usr/share/common-licenses/Artistic'.
